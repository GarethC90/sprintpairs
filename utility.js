function pageSetup(){
	disablePotUpdates();
}

function clickEvents(){
	$("#genPairs").click(function(){
		generatePairs();
	});

	$("#showPots").click(function(){
		enablePotUpdates();
		$('#potA').val(getPotContents("A"));
		$('#potB').val(getPotContents("B"));
	});

	$("#updatePots").click(function(){
		disablePotUpdates();
		updatePots();
	});
}

function disablePotUpdates(){
	$(".pot").attr("disabled", true);
	$("#updatePots").hide();
	$("#pairs").show();
}

function enablePotUpdates(){
	$(".pot").attr("disabled", false);
	$("#updatePots").show();
	$("#pairs").hide();
}

//Shuffle the members in an array randomly
function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}