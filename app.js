$(document).ready(function(){
	pageSetup();
	clickEvents();
}); 

function getPotContents(potLetter){
	return (localStorage.getItem("pot"+potLetter).replace(/,/g, '\n').replace(/[\[\]"]+/g, ''));
}

function updatePots(){
	localStorage.potA = JSON.stringify(extractMembersFromPot("A"));
	localStorage.potB = JSON.stringify(extractMembersFromPot("B"));
}

function generatePairs(){

	if(arePotsAvailable()){
		
		var potAMembers = JSON.parse(localStorage.getItem("potA")), potBMembers = JSON.parse(localStorage.getItem("potB"));
		var equalPotSize = (potAMembers.length === potBMembers.length);
		var pairs = [];

		if(equalPotSize){

			shuffle(potAMembers);
			shuffle(potBMembers);
			$("#pairsList").empty();

			for(var i = 0; i < potAMembers.length; i++){
				var pair = {
					one: potAMembers[i],
					two: potBMembers[i]
				}

				pairs.push(pair);
				$("#pairsList").append("<li>"+pair.one+" & "+pair.two+"</li>");
			}
			localStorage.pairs = JSON.stringify(pairs);
		}else{
			alert("Sorry, both pots must have the same amount of team members");
		}
	}

}

function arePotsAvailable(){
	return (localStorage.getItem("potA") !== null && localStorage.getItem("potB") !== null);
}

function extractMembersFromPot(potLetter){
	var pot = $('#pot'+potLetter).val().split(/\n/);
	var potMembers = [];
	for (var i=0; i < pot.length; i++) {
	  // only push this line if it contains a non whitespace character.
	  if (/\S/.test(pot[i])) {
	    potMembers.push($.trim(pot[i]));
	  }
	}

	return potMembers;
}